$(function () {

    // SCROLL TOP START
    $(window).scroll(function () {
        var n = $('html, body').scrollTop();
        if (n >= 100) {
            $('.header').addClass('active');
            $('.scroll-top-btn').addClass('active');
        }
        else {
            $('.header').removeClass('active');
            $('.scroll-top-btn').removeClass('active');

        }
    });
    // SCROLL TOP
    $('.scroll-top-btn').on('click', function (event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 1000)
    })
    // SCROLL TOP END

    // TOGGLE HEADER MENU START
    $('#open-header-menu').click(function (e) {
        e.preventDefault();
        $('.header-menu__small-display').toggleClass('active');
    });
    // TOGGLE HEADER MENU END

    // TOGGLE SEARCH FORM START
    $("#search").click(function (e) {
        e.preventDefault();
        $('#search-form').addClass('active');
    });
    $('#search-form-close').click(function (e) {
        e.preventDefault();
        $('#search-form').removeClass('active');
    });
    // TOGGLE SEARCH FORM END

    // TOGGLE SIDEBAR START
    $('#menu-bar').click(function (e) {
        e.preventDefault();
        $('.sidebar__container').addClass('active');
    });
    $('#sidebar-close').click(function (e) {
        e.preventDefault();
        $('.sidebar__container').removeClass('active');
    });
    // TOGGLE SIDEBAR END

    // TOGGLE POPUP VIDEO START
    $('#open-popup-video').click(function (e) {
        e.preventDefault();
        $('#modal-video').addClass('active');
    });
    $('#modal-video-close').click(function (e) {
        e.preventDefault();
        $('#modal-video').removeClass('active');
    });
    // TOGGLE POPUP VIDEO END

    // TOGGLE ACCORDION START
    $('.accordion-button').click(function (e) {
        e.preventDefault();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        }
        else {
            $('.accordion-button.active').removeClass('active');
            $(this).addClass('active');
        }
    });
    $('.checkout__coupon').slideUp();
    $('#open_code').click(function (e) {
        $('.checkout__coupon').slideToggle();

    })
    // TOGGLE ACCORDION END

    // SLICK SLIDER START

    $('.main__slide-home-wraps ').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        // dots: true,
        arrows: false,
        cssEase: "ease",
        speed: 1500,
        easing: 'linear',

    });

    $('.banner-slide__body').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 2,
        slidesToScroll: 2,
        dots: true,
        arrows: false,
        responsive: [

            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });


    $('.expert-slick').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        cssEase: "ease",
        speed: 1500,
        easing: 'linear',
        slidesToShow: 3,
        // slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },

            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]

    });
    $('.news-slick').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        cssEase: "ease",
        speed: 1500,
        easing: 'linear',
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('.banner-slide__body').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
        // arrows: false,
        cssEase: "ease",
        speed: 1500,
        easing: 'linear',
        slidesToShow: 2,
        // responsive: [
        //     {
        //         breakpoint: 1200,
        //         settings: {
        //             slidesToShow: 2,
        //             slidesToScroll: 1,
        //             infinite: true,
        //             dots: true
        //         }
        //     },
        //     {
        //         breakpoint: 1000,
        //         settings: {
        //             slidesToShow: 1,
        //             slidesToScroll: 1
        //         }
        //     }
        // ]

    });

    $('.logo__slide').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        // dots: true,
        arrows: false,
        cssEase: "ease",
        speed: 1500,
        easing: 'linear',
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: true,
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 1400,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 300,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.banner-slide__slick').slick(
        {
            autoplay: true,
            autoplaySpeed: 2000,
            // dots: true,
            arrows: false,
            cssEase: "ease",
            speed: 1500,
            easing: 'linear',
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,

            responsive: [

                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        }
    );

    // SLICK SLIDE END

    // SET VIEW BLOG START
    $('.course__btn-inner i').click(function (e) {
        e.preventDefault();
        $('i.active').removeClass('active');
        $(this).addClass('active')
    });
    $('#list').click(function (e) {
        e.preventDefault();
        $('.courses-item').removeClass('col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 wraps');
        $('.courses-item').addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12');

    });
    $('#large').click(function (e) {
        e.preventDefault();
        $('.courses-item').addClass('col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 wraps');
        $('.courses-item').removeClass('col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12');
    });
    // SET VIEW BLOG END

    // FANCY START
    $('#popup-video').fancybox();
    $('.gallery__inner a').fancybox();
    $('.course-single__link').fancybox();
    $('.sidebar__img a').fancybox();
});